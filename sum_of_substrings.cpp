#include <iostream>

// https://codeforces.com/problemset/problem/1691/C

int f(std::string s) {
  int sum = 0;
  if (s[0] == '1') {
    sum += 10;
  }

  for (int i = 1; i < s.length() - 1; i++) {
    sum += s[ i ] == '1' ? 11 : 0;
  }

  if (s[ s.length() - 1 ] == '1') {
    sum += 1;
  }

  return sum;
}

void solve() {

  int n;
  int k;
  std::cin >> n;
  std::cin >> k;

  std::string s;
  std::cin >> s;

  int last_id = s.length() - 1;

  // first optimization, carry 1 to the right

  int required_k = 0;
  bool some_1 = false;
  for (int i = last_id; i >= 0; i--) {
    if (s[ i ] == '1') {
      some_1 = true;
      break;
    }

    required_k++;
  }

  if (s[ last_id ] == '0' && some_1 && k >= required_k) {
    std::swap(s[ last_id ], s[ last_id - required_k ]);
    k -= required_k;
  }

  // second optimization, carry 1 to left
  required_k = 0;
  some_1 = false;
  for (int i = 0; i < s.length() - 1; i++) {
    if (s[ i ] == '1') {
      some_1 = true;
      break;
    }
    required_k++;
  }

  if (s[ 0 ] == '0' && some_1 && k >= required_k) {
    std::swap(s[ 0 ], s[ required_k ]);
  }

  std::cout << f(s) << std::endl;
}

int main(int argc, char** argv) {

  std::ios_base::sync_with_stdio(false);
  std::cin.tie(0);
  std::cout.tie(0);

  int t;
  std::cin >> t;
  
  while (t-->0) {
    solve();
  }

  return 0;
}
