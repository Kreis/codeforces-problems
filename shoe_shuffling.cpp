#include <iostream>
#include <map>
#include <vector>

void solve() {

  int n;
  std::cin >> n;

  std::map<int, int> mm;

  for (int i = 0; i < n; i++) {
    int tt;
    std::cin >> tt;

    mm[ tt ]++;
  }

  std::vector<int> result(n);
  int ri = 0;

  bool doesnt_exist = false;
  int count = 0;
  for (auto i : mm) {
    if (i.second == 1) {
      doesnt_exist = true;
    }

    result[ ri++ ] = count + i.second;
    for (int j = 0; j < i.second - 1; j++) {
      result[ ri++ ] = count + j + 1;
    }

    count += i.second;
  }

  if (doesnt_exist) {
    std::cout << "-1" << std::endl;
  } else {
    for (int i = 0; i < n; i++) {
      std::cout << result[ i ] << " ";
    }
    std::cout << std::endl;
  }
}

int main(int argc, char** argv) {

  int t;
  std::cin >> t;

  while (t-->0) {
    solve();
  }

  return 0;
}

