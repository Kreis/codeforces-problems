#include <iostream>
#include <map>
#include <vector>

// problem
// https://codeforces.com/problemset/problem/1541/B

int main(int argc, char** argv) {

  // getting primes of relative N
  uint32_t inf = 200002;
  std::vector<std::vector<uint32_t> > primes(inf);
  for (uint32_t i = 1; i < inf; i++) {
    for (uint32_t j = i; j < inf; j += i) {
      primes[ j ].push_back(i);
    }
  }

  uint32_t t; std::cin >> t;

  while (t --> 0) {
    uint32_t N; std::cin >> N;
    std::map<uint32_t, uint32_t> nums;
    
    for (uint32_t i = 1; i <= N; i++) {
      uint32_t tmp;
      std::cin >> tmp;
      nums[ tmp ] = i;
    }
    
    uint32_t ans = 0;
    for (uint32_t i = 2; i <= 2*N; i++) {
      for (uint32_t p : primes[ i ]) {
        if (p >= i / p) {
          break;
        }

        auto ptr1 = nums.find(p);
        auto ptr2 = nums.find(i / p);
        if (
          ptr1 != nums.end() &&
          ptr2 != nums.end() &&
          ptr1->second + ptr2->second == i
          ) {
          ans++;
        }
      }
    }

    std::cout << ans << std::endl;
  }


  return 0;
}
