#include <iostream>
#include <vector>

// problem
// https://codeforces.com/problemset/problem/1676/G
 
int dfs(int index,
          std::string& colors,
          std::vector<std::vector<int> >& childs,
          int& total) {
 
  int my_value = colors[ index ] == 'W' ? 1 : -1;
 
  for (int i = 0; i < childs[ index ].size(); i++) {
    my_value += dfs(childs[ index][ i ], colors, childs, total); 
  }
  
  if (my_value == 0) {
    total += 1;
  }
 
  return my_value;
}
 
int main(int argc, char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
 
  int t;
  std::cin >> t;
 
  while (t-->0) {
    int n;
    std::cin >> n;
    std::vector<std::vector<int> > childs(n);
    for (int i = 0; i < n - 1; i++) {
      int x;
      std::cin >> x;
      childs[ x - 1 ].push_back(i + 1);
    }
 
    std::string colors;
    std::cin >> colors;
 
    int total = 0;
    dfs(0, colors, childs, total);
    std::cout << total << std::endl;
 
  }
 
  return 0;
}

