#include <iostream>
#include <vector>
 
typedef unsigned long long int ull;

// https://codeforces.com/problemset/problem/1679/B
 
int main(int argc, char** argv) {
 
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(0);
 
  ull n;
  ull q;
  std::cin >> n;
  std::cin >> q;
 
  std::vector<ull> elements(n, 0);
  std::vector<ull> version(n, 0);
  ull global_value = 0;
  ull version_value = 0;
  ull total_value = 0;
  for (ull i = 0; i < n; i++) {
    std::cin >> elements[ i ];
    total_value += elements[ i ];
  }
 
 
  for (ull i = 0; i < q; i++) {
    ull t;
    std::cin >> t;
 
    if (t == 1) {
      ull ii;
      ull xx;
      std::cin >> ii;
      std::cin >> xx;
 
      // if is not updated, use the last global_value
      if (version[ ii - 1 ] < version_value) {
        total_value -= global_value;
        total_value += xx;

        elements[ ii - 1 ] = xx;
        version[ ii - 1 ] = version_value;
      } else {
      // else use the updated value
        total_value -= elements[ ii - 1 ];
        total_value += xx;

        elements[ ii - 1 ] = xx;
      }
 
      std::cout << total_value << std::endl;
    }
 
    if (t == 2) {
      ull xx;
      std::cin >> xx;
      
      global_value = xx;
      version_value++;
      total_value = xx * n;
 
      std::cout << total_value << std::endl;
    }
 
  }
  
 
  return 0;
}
 
 
