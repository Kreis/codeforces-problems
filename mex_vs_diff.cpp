#include <iostream>
#include <queue>
#include <vector>
#include <set>
#include <map>
#include <algorithm>

// https://codeforces.com/problemset/problem/1684/E

class Node {
public:
  Node(int n, int counter):
    n(n), counter(counter) {}

  int n;
  int counter;

  struct CmpNode {
    bool operator()(const Node& a, const Node& b) {
      if (a.counter > b.counter) {
        return true;
      }
      if (a.counter < b.counter) {
        return false;
      }
      return a.n < b.n;
    }
  };
};


void solve() {

  int n;
  int k;
  std::cin >> n;
  std::cin >> k;

  std::map<int, int> mapa;
  std::vector<bool> veca(n, false);
  for (int i = 0; i < n; i++) {
    int tt;
    std::cin >> tt;
    mapa[ tt ]++;

    if (tt < n) {
      veca[ tt ] = true;
    }
  }

  std::priority_queue<Node, std::vector<Node>, Node::CmpNode> afterK;

  // find the k gap
  int gap = 0;
  bool findit = false;
  int kk = k;
  int countk = 0;
  for (int i = 0; i < veca.size(); i++) {
    if ( ! veca[ i ]) {

      if (kk == 0) {
        findit = true;
        gap = i;
        break;
      }

      countk++;
      kk--;
    }
  }
  if ( ! findit) {
    gap = n;
  }


  for (const auto& i : mapa) {
    if (i.first >= gap) {
      afterK.push(Node(i.first, i.second));
    }
  }

  while (k > 0 && ! afterK.empty()) {
    Node c = afterK.top();
    afterK.pop();
    mapa[ c.n ]--;
    if (mapa[ c.n ] == 0) {
      mapa.erase(c.n);
    }

    k--;

    if (c.counter > 1) {
      afterK.push(Node(c.n, c.counter - 1));
    }
  }

  std::cout << (int)mapa.size() + countk - gap << std::endl;
}

int main(int argc, char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(0);

  int t;
  std::cin >> t;
  while (t-->0) {
    solve();
  }
  return 0;
}

